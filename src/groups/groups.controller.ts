import { Request, Response } from 'express';
import * as groupsService from './groups.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IGroupUpdateRequest } from './types/group-update-request-interface';
import { IGroupCreateRequest } from './types/group-create-request-interface';
import { IStudent } from '../students/types/student.interface';
import { IGroup } from './types/group.interface';
import { HttpStatuses } from '../application/enums/http-statuses.enum';

export const getAllGroups = (request: Request, response: Response) => {
  const groups = groupsService.getAllGroups();
  type GroupWithStudents = IGroup & {
    students: Array<Omit<IStudent, 'groupId'>>;
  };
  const groupsWithStudents: Array<GroupWithStudents> = [];
  groups.forEach((group) => {
    groupsWithStudents.push(groupsService.getGroupWithStudents(group));
  });
  response.json(groupsWithStudents);
};

export const getGroupById = (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const group = groupsService.getGroupById(id);
  const groupAndStudents = groupsService.getGroupWithStudents(group);
  response.json(groupAndStudents);
};

export const createGroup = (
  request: ValidatedRequest<IGroupCreateRequest>,
  response: Response,
) => {
  const group = groupsService.createGroup(request.body);
  response.statusCode = HttpStatuses.CREATED;
  response.json(group);
};

export const updateGroupById = (
  request: ValidatedRequest<IGroupUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const group = groupsService.updateGroupById(id, request.body);
  response.json(group);
};

export const deleteGroupById = (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const group = groupsService.deleteGroupById(id);
  response.json(group);
};
