import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import * as groupsModel from './groups.model';
import { IGroup } from './types/group.interface';
import { IStudent } from '../students/types/student.interface';
import { getAllStudents } from '../students/students.service';

export const getGroupWithStudents = (group: IGroup) => {
  const studentsOfGroup: Array<Omit<IStudent, 'groupId'>> = [];
  const students = getAllStudents();
  students.forEach((student) => {
    if (group.id === student.groupId) {
      const { groupId, ...restStudentInfo } = student;
      studentsOfGroup.push(restStudentInfo);
    }
  });
  return { ...group, students: studentsOfGroup };
};

export const getAllGroups = () => {
  return groupsModel.getAllGroups();
};

export const getGroupById = (id: string) => {
  const group = groupsModel.getGroupById(id);
  if (!group) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
  }
  return group;
};

export const createGroup = (creategroupschema: Omit<IGroup, 'id'>) => {
  return groupsModel.createGroup(creategroupschema);
};

export const updateGroupById = (
  id: string,
  updategroupschema: Partial<IGroup>,
) => {
  return groupsModel.updateGroupById(id, updategroupschema);
};

export const deleteGroupById = (id: string) => {
  return groupsModel.deleteGroupById(id);
};
