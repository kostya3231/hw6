import cors from 'cors';
import express from 'express';
import logger from './middlewares/logger.middleware';
import studentsRouter from '../students/students.router';
import groupsRouter from '../groups/groups.router';
import bodyParser from 'body-parser';
import exceptionsFilter from './middlewares/exceptions.filter';
import path from 'path';

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(logger);

const staticFilesPath = path.join(__dirname, '../', 'public');

app.use('/api/v1/public', express.static(staticFilesPath));
app.use('/api/v1/students', studentsRouter);
app.use('/api/v1/groups', groupsRouter);

app.use(exceptionsFilter);

export default app;
