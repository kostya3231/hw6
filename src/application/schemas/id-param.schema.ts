import Joi from 'joi';

export const idParamSchema = Joi.object<{ id: string; groupId?: string }>({
  id: Joi.string().hex().length(24).required(),
  groupId: Joi.string().hex().length(24).optional(),
});
