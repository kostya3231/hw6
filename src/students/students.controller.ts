import { Request, Response } from 'express';
import * as studentsService from './students.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IStudentUpdateRequest } from './types/student-update-request-interface';
import { IStudentCreateRequest } from './types/student-create-request-interface';
import { IStudent } from './types/student.interface';
import { HttpStatuses } from '../application/enums/http-statuses.enum';

export const getAllStudents = (request: Request, response: Response) => {
  const students = studentsService.getAllStudents();
  type StudentAndGroupName = Omit<IStudent, 'groupId'> & { groupName?: string };
  const studentsAndGroups: Array<StudentAndGroupName> = [];
  students.forEach((student) => {
    studentsAndGroups.push(studentsService.getStudentAndGroupName(student));
  });
  response.json(studentsAndGroups);
};

export const getStudentById = (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const student = studentsService.getStudentById(id);
  const studentAndGroupName = studentsService.getStudentAndGroupName(student);
  response.json(studentAndGroupName);
};

export const createStudent = (
  request: ValidatedRequest<IStudentCreateRequest>,
  response: Response,
) => {
  const student = studentsService.createStudent(request.body);
  response.statusCode = HttpStatuses.CREATED;
  response.json(student);
};

export const updateStudentById = (
  request: ValidatedRequest<IStudentUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const student = studentsService.updateStudentById(id, request.body);
  response.json(student);
};

export const addGroup = (
  request: Request<{ id: string; groupId: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const { groupId } = request.params;
  const student = studentsService.addGroup(id, groupId);
  response.json(student);
};

export const addImage = (
  request: Request<{ id: string; file: Express.Multer.File }>,
  response: Response,
) => {
  const { id } = request.params;
  const { path } = request.file ?? {};
  const student = studentsService.addImage(id, path);
  response.json(student);
};

export const deleteStudentById = (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const student = studentsService.deleteStudentById(id);
  response.json(student);
};
