import ObjectID from 'bson-objectid';
import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import * as studentsModel from './students.model';
import { getGroupById } from '../groups/groups.model';
import { IStudent } from './types/student.interface';
import path from 'path';
import fs from 'fs/promises';
import { IGroup } from '../groups/types/group.interface';

export const getStudentAndGroupName = (student: IStudent) => {
  const { groupId, ...studentRest } = student;
  if (groupId) {
    const group = getGroupById(groupId) as IGroup;
    return { ...studentRest, groupName: group.name };
  } else {
    return { ...studentRest, groupName: '' };
  }
};

export const getAllStudents = () => {
  return studentsModel.getAllStudents();
};

export const getStudentById = (id: string) => {
  const student = studentsModel.getStudentById(id);
  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }
  return student;
};

export const createStudent = (createStudentSchema: Omit<IStudent, 'id'>) => {
  return studentsModel.createStudent(createStudentSchema);
};

export const updateStudentById = (
  id: string,
  updateStudentSchema: Partial<IStudent>,
) => {
  return studentsModel.updateStudentById(id, updateStudentSchema);
};

export const addGroup = (id: string, groupId: string) => {
  const group = getGroupById(groupId);
  if (!group) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
  }
  const updatedStudent = updateStudentById(id, { groupId });
  return updatedStudent;
};

export const addImage = async (id: string, filePath?: string) => {
  if (!filePath) {
    throw new HttpException(HttpStatuses.BAD_REQUEST, 'File is not provided');
  }

  try {
    const imageId = ObjectID().toHexString();
    const imageExtension = path.extname(filePath);
    const imageName = imageId + imageExtension;

    const studentsDirectoyName = 'students';
    const studentsDirectoyPath = path.join(
      __dirname,
      '../',
      'public',
      studentsDirectoyName,
    );
    const newImagePath = path.join(studentsDirectoyPath, imageName);
    const imagePath = `${studentsDirectoyName}/${imageName}`;

    await fs.rename(filePath, newImagePath);

    const updatedStudent = updateStudentById(id, { imagePath });

    return updatedStudent;
  } catch (error) {
    await fs.unlink(filePath);
    throw error;
  }
};

export const deleteStudentById = (id: string) => {
  return studentsModel.deleteStudentById(id);
};
